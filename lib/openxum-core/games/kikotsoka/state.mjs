"use strict";

const State = {
  VACANT: 0,
  BLACK: 1,
  WHITE: 2,
  BLACK_BLOCKED: 3,
  WHITE_BLOCKED: 4,
  BLOCKED: 5,
  to_string(s) {
    if (s === State.VACANT) { return '[ ]'; }
    if (s === State.BLACK) { return '[B]'; }
    if (s === State.WHITE) { return '[W]'; }
    if (s === State.BLACK_BLOCKED) { return ' b '; }
    if (s === State.WHITE_BLOCKED) { return ' w '; }
    if (s === State.BLOCKED) { return '   '; }
  }
};

export default State;